const 	gulp 	 = require('gulp'),
		del  	 = require('del'),
		imagemin = require('gulp-imagemin')

gulp.task('removedist', function() { return del.sync('dist');});

gulp.task('build', ['removedist', 'htmlIncluder', 'styles', 'js'], function() {

	var buildFonts = gulp.src([
		'app/fonts/**/*',
		]).pipe(gulp.dest('dist/fonts'));

	var buildCss = gulp.src([
		'app/css/main.min.css',
		]).pipe(gulp.dest('dist/css'));

	var buildJs = gulp.src([
		'app/js/scripts.min.js',
		]).pipe(gulp.dest('dist/js'));

	var buildLibs = gulp.src([
		'app/libs/**',
		]).pipe(gulp.dest('dist/libs'));
	
	var buildImg = gulp.src([
		'app/img/**/*',
		])
		.pipe(imagemin())
		.pipe(gulp.dest('dist/img'));
		
});