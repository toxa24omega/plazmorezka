const	syntax = 'sass';
const	gulp = require('gulp'),
		browserSync = require('browser-sync')


gulp.task('watch', ['styles', 'js', 'browser-sync'], function() {
	gulp.watch('app/'+syntax+'/**/*.'+syntax+'', ['styles']);
	gulp.watch('app/js/common.js', ['js']);
	gulp.watch('app/**/*.html', ['htmlIncluder']).on('change', browserSync.reload);
});