const syntax = 'sass';

const   gulp          = require('gulp'),
        sass          = require('gulp-sass'),
        cleancss      = require('gulp-clean-css'),
        rename        = require('gulp-rename'),
        autoprefixer  = require('gulp-autoprefixer'),
        notify        = require('gulp-notify'),
        sourcemaps    = require('gulp-sourcemaps'),
        browserSync   = require('browser-sync')

gulp.task('styles', function() {
    return gulp.src('app/'+syntax+'/**/*.'+syntax+'')
    // .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
    .pipe(rename({ suffix: '.min', prefix : '' }))
    .pipe(autoprefixer(['last 2 versions']))
    .pipe(cleancss( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
    // .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.reload({ stream: true }))
});