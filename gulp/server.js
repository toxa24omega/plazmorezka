const 	gulp        = require('gulp'),
		browserSync = require('browser-sync')

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'dist/'
		},
		open: false,
		online: false // Work Offline Without Internet Connection
		// tunnel: true, tunnel: "projectname", // Demonstration page: http://projectname.localtunnel.me
	})
});