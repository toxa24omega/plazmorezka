const 	gulp   = require('gulp'),
	babel  = require('gulp-babel'),
        concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	browserSync = require('browser-sync')

gulp.task('js', function() {
	return gulp.src([
	// 'app/libs/jquery/dist/jquery.min.js',
	'app/js/common.js', // Always at the end
	])
	.pipe(babel({presets: ['@babel/env']}))
	.pipe(concat('scripts.min.js'))
	.pipe(uglify()) // Mifify js (opt.)
	.pipe(gulp.dest('dist/js/'))
	.pipe(browserSync.reload({ stream: true }))
});