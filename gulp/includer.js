const 	gulp 	 = require('gulp'),
        includer = require('gulp-htmlincluder'),
        htmlmin  = require('gulp-htmlmin'),
        browserSync = require('browser-sync')

gulp.task('htmlIncluder', function() {
    gulp.src('app/**/*.html')
	.pipe(includer())
	// .pipe(htmlmin({collapseWhitespace: false, removeComments: true}))
	.pipe(gulp.dest('dist/'))
	// .pipe(browserSync.reload({ stream: true }))
});